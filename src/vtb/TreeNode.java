package vtb;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class TreeNode {
    private TreeNode parent;
    private List<TreeNode> children;
    private Object data;

    public static int countLeafs(TreeNode root) {
        if(root == null) {
            return 0;
        } else if (root.children == null) {
            return 1;
        }
        return root.children.stream()
                .mapToInt(TreeNode::countLeafs)
                .sum();
    }
    public TreeNode getParent() {
        return parent;
    }

    public void setParent(TreeNode parent) {
        this.parent = parent;
    }

    public TreeNode getRoot() {
        TreeNode root = getParent();
        if(root != null) {
            while(root.getParent() != null) {
                root = root.getParent();
            }
        }
        return root;
    }

    public boolean isLeaf() {
        return children == null || children.size() == 0;
    }

    public int getChildCount() {
        return isLeaf() ? 0 : children.size();
    }

    public Iterator<TreeNode> getChildrenIterator() {
        return children.iterator();
    }

    public void addChild(TreeNode child) {
        if(isLeaf()) {
            children = new LinkedList<>();
        }
        children.add(child);
        child.setParent(this);
    }

    public boolean removeChild(TreeNode child) {
        Iterator<TreeNode> iterator = getChildrenIterator();
        TreeNode childNode;
        while(iterator.hasNext()) {
            if((childNode = iterator.next()).equals(child)) {
                childNode.setParent(null);
                iterator.remove();
                return true;
            }
        }
        return false;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getTreePath() {
        StringBuilder treePath = new StringBuilder(getData() == null ? "empty" : getData().toString());
        if(getParent() != null) {
            return treePath.insert(0, getParent().getTreePath() + "->").toString();
        }
        return treePath.toString();
    }

    public TreeNode findParent(Object data) {
        if((getData() == null && data == null) || (getData() != null && getData().equals(data))) {
            return this;
        }
        if(getParent() != null) {
            return getParent().findParent(data);
        }
        return null;
    }

    public TreeNode findChild(Object data) {
        if(children != null) {
            for(TreeNode child : children) {
                if((data == null && child.getData() == null) ||
                        (child.getData() != null && child.getData().equals(data)) ||
                        ((child = child.findChild(data)) != null)) {
                    return child;
                }
            }
        }
        return null;
    }
}