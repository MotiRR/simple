package vtb;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Main {


    /*
    1. Есть список занятых ip адресов в определенном диапазоне. Необходимо
    возвратить список свободных ip-адресов в этом диапазоне
    */
    static List<String> formRecord(HashMap<String, String> hashMap) {
        StringBuilder stringBuilder;
        ArrayList<String> arrayList = new ArrayList<>();
        ArrayList<Integer> octava, octcopy;
        octava = new ArrayList<>();
        for (int i = 0; i < 256; i++) octava.add(i);
        String[] key, value;
        int ivalue = 0;
        for (Map.Entry<String, String> pair : hashMap.entrySet()) {
            octcopy = new ArrayList<>(octava);
            stringBuilder = new StringBuilder();
            stringBuilder.append(pair.getKey());
            stringBuilder.append(".");
            key = pair.getKey().split("\\.");
            value = pair.getValue().trim().split("\\s");
            for (String v : value) {
                ivalue = Integer.parseInt(v);
                octcopy.set(octava.indexOf(ivalue), -1);
            }
            String str;
            for (Integer val : octcopy) {
                if (val >= 0) {
                    str = stringBuilder.toString() + val;
                    arrayList.add(str);
                }
            }

        }
        return arrayList;
    }

    static void fill(HashMap<String, String> hashMap, String... str) {
        String key;
        key = str[0];
        for (int i = 1; i < str.length - 1; i++) {
            key = key + "." + str[i];
        }
        if (hashMap.containsKey(key)) hashMap.put(key, hashMap.get(key).concat(" " + str[str.length - 1]));
        else
            hashMap.put(key, str[str.length - 1]);
    }

    static List<String> searchIP(List<String> ipAddress) {
        Pattern pattern;
        Matcher matcher;
        HashMap<String, String> hashMap = new HashMap<>();

        TreeSet<String> ts = new TreeSet<>(ipAddress);
        String[] currentIP, previousIP = new String[4];
        pattern = Pattern.compile("\\d+\\.\\d+\\.\\d+.\\d+");
        boolean first = true;
        String key;
        for (String s : ts) {
            matcher = pattern.matcher(s);
            if (matcher.matches()) {
                if (first) {
                    previousIP = s.split("\\.");
                    first = false;
                    continue;
                } else {
                    currentIP = s.split("\\.");
                    if (currentIP[0].equalsIgnoreCase(previousIP[0]))
                        if (currentIP[1].equalsIgnoreCase(previousIP[1])) {
                            if (currentIP[2].equalsIgnoreCase(previousIP[2])) {
                                if (ts.last().equalsIgnoreCase(s)) {
                                    fill(hashMap, previousIP[0], previousIP[1], previousIP[2], previousIP[3]);
                                    fill(hashMap, currentIP[0], currentIP[1], currentIP[2], currentIP[3]);
                                } else
                                    fill(hashMap, currentIP[0], currentIP[1], currentIP[2], currentIP[3]);
                            } else {
                                if (ts.last().equalsIgnoreCase(s)) {
                                    fill(hashMap, previousIP[0], previousIP[1], previousIP[2], previousIP[3]);
                                    fill(hashMap, currentIP[0], currentIP[1], currentIP[2], previousIP[3]);
                                } else fill(hashMap, currentIP[0], currentIP[1], currentIP[2], currentIP[3]);
                            }
                        } else {
                            if (ts.last().equalsIgnoreCase(s)) {
                                fill(hashMap, previousIP[0], previousIP[1], previousIP[2]);
                                fill(hashMap, currentIP[0], currentIP[1], previousIP[2]);
                            } else fill(hashMap, currentIP[0], currentIP[1], currentIP[2]);
                        }

                }

                previousIP = currentIP;
            } else System.out.println(("Формат должен быть d{1-3}.d{1-3}.d{1-3}.d{1-3}, " +
                    "неправильный формат в записи ").concat(s));
        }

        System.out.println("Сейчас как верну со всей силы вот эту HashMap!!!!");
        System.out.println("hashMap = {");
        for (Map.Entry<String, String> entry : hashMap.entrySet()) {
            System.out.println("[" + entry.getKey() + " : " + entry.getValue() + "],");
        }
        System.out.println("\b\b}");
        return formRecord(hashMap);
    }


    /*
    2. Написать функцию, которая принимает на вход строку и возвращает наиболее
    часто встречающееся в этой строке слово
    */
    static String mostFound(String str) {
        long max = 0;
        String key = "";
        System.out.println(str);
        Pattern pattern = Pattern.compile("\\s+");
        String[] strs = pattern.split(str);
        Map<String, List<String>> collect = Arrays.stream(strs).collect(Collectors.groupingBy(String::intern));

        for (Map.Entry<String, List<String>> pair : collect.entrySet()) {
            long count = pair.getValue().size();
            if (max < count) {
                max = count;
                key = pair.getKey();
            }
        }
        return key;
    }

    /*
    3. Написать функцию, которая принимает на вход строку и формат даты, и
    возвращает список найденных дат в заданной строке, соответствующих заданному формату
     */
    static List<String> searchDate(String str, String formatDate) {
        String substring;
        ArrayList<String> arrayList = new ArrayList<>();
        if (str.length() < formatDate.length()) return arrayList;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(formatDate);
        int i = 0;
        int lengthPattern = formatDate.length();
        while ((i + lengthPattern - 1) < str.length()) {
            substring = str.substring(i, i + lengthPattern);
            try {
                LocalDate.parse(substring, formatter);
                arrayList.add(substring);
            } catch (DateTimeParseException ex) {
                //ex.printStackTrace();
            } finally {
                ++i;
            }
        }
        return arrayList;
    }

    /*
    4. Написать функцию, которая принимает на вход строку, и возвращает сумму всех
    найденных в этой строке чисел
     */
    static long calculateAmount(String str) {
        Pattern pattern = Pattern.compile("\\s*\\D");
        String[] strs = pattern.split(str);
        System.out.println(str);
        Long aLong = Arrays.stream(strs).filter(s -> !s.isEmpty()).map(Long::parseLong).reduce(Long::sum).orElse(0L);
        return aLong;
    }


    /*
    5. Данные хранятся в структуре в виде дерева. Напишите метод, подсчитывающий
    количество «листьев» (конечных элементов) в дереве
     */
    static void calculateLeaves() {
        TreeNode treeNodeForTest = new TreeNode();
        TreeNode treeNodeForTest1 = new TreeNode();
        TreeNode treeNodeForTest2 = new TreeNode();
        TreeNode treeNodeForTest3 = new TreeNode();
        TreeNode treeNodeForTest4 = new TreeNode();
        TreeNode treeNodeForTest5 = new TreeNode();
        TreeNode treeNodeForTest6 = new TreeNode();
        TreeNode treeNodeForTest7 = new TreeNode();

        System.out.println(TreeNode.countLeafs(treeNodeForTest));

        treeNodeForTest.addChild(treeNodeForTest1);
        System.out.println(TreeNode.countLeafs(treeNodeForTest));
        treeNodeForTest.addChild(treeNodeForTest2);
        System.out.println(TreeNode.countLeafs(treeNodeForTest));

        treeNodeForTest1.addChild(treeNodeForTest3);
        System.out.println(TreeNode.countLeafs(treeNodeForTest));

        treeNodeForTest.addChild(treeNodeForTest4);
        System.out.println(TreeNode.countLeafs(treeNodeForTest));

        treeNodeForTest1.addChild(treeNodeForTest5);
        treeNodeForTest1.addChild(treeNodeForTest6);
        System.out.println(TreeNode.countLeafs(treeNodeForTest));

        treeNodeForTest6.addChild(treeNodeForTest7);
        System.out.println(TreeNode.countLeafs(treeNodeForTest));

        treeNodeForTest.removeChild(treeNodeForTest1);
        System.out.println(TreeNode.countLeafs(treeNodeForTest));
    }


    public static void main(String[] args) {
        System.out.println(searchIP(Arrays.asList("192.168.0.1", "192.168.0.2", "192.168.0.3")).toString());
        System.out.println(mostFound("hello hello buy"));
        System.out.println(calculateAmount("fg 234 fgd 34f4fd sdf3 ff23"));
        System.out.println(searchDate("today 1969-07-16 or 1969-07-17", "yyyy-MM-dd"));
        calculateLeaves();
    }
}
